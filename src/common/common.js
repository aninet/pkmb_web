
function resCodeJudge(code) {
    let permission = false;
    let mesTip = "";
    if(code == "E000008" ) {
        permission = true;
        mesTip = "未登录"
    }
    if( code == "E000009" || code == "E000012" || code == "E000013" || code == "E000014") {
        permission = true;
        mesTip = "登录失效"
    }
    return {
        permission: permission,
        mesTip: mesTip
    }
}

function resData(res) {
    let result = null;
    if( res.data.code == "00" && res.data.data != null) {
        result = res.data.data
    }else {
        if(!resCodeJudge(res.data.code).permission) {
            this.$toast.fail(res.data.msg)
        }
    }
    return result
}

function resResult(res) {
    if( res.data.code == "00") {
        return true
    }else {
        if(!resCodeJudge(res.data.code).permission) {
            this.$toast.fail(res.data.msg)
        }
        return false
    }
}

//处理列表数据
function resArr(res) {
    let result = null;
    if( res.data.code == "00" && res.data.data != null) {
        if(Array.isArray(res.data.data.list)) {
            result = res.data.data
        }else {
            this.$toast.fail("未正确返回列表数据")
        }
    }else {
        if(!resCodeJudge(res.data.code).permission) {
            this.$toast.fail(res.data.msg)
        }
    }
    return result
}

function delaySubmit(mes,callBack) {
    mes = mes ? mes : "加载中...";
    this.$toast.loading({
        message: mes,
        forbidClick: true,
    });
    setTimeout(()=> {
        callBack()
    },300)
}


/*
* 默认接受参数 Sting ,返回上一页的地址
* vuex 中维护上一页地址数据
*
* 注：相较于维护整个历史更简单，如果页面需要层级跳转，在所需要的页面进行相关设置
* 层级跳转示例
* 假使当前轨迹为：z->a->b->c->d，d为当前路由
*后backward()或者go(-1)到c，路由轨迹变为：z->a->b->c
*再push到a，路由轨迹不是变成z->a->b->c->a，而是z->a
*再push到b，路由轨迹变为：z->a->b
*这个记录是不允许重复的，出现重复则轨迹后退到历史的某一个点。
* 如果后期业务复杂到需要将层级跳转抽象维护，可参考 https://juejin.im/post/5bd44d70f265da0aa74f7073
 */
function navBack(val) {
    /*
    * 传值，返回指定页面
    * 传特定字符串 ："prePath",返回上一页, 用于跳回不同的页面
    * 指定跳回之前,应该在路由守卫(离开前)中设置要跳回的页面,即维护vuex 中的prePath
    * 没有上一页， 返回首页
    * */
    //页面中引用的会默认传参 mouseEvent
    let url = null;
    if(typeof val === "string") {
        url = val
    }
    if(!this.$router) {
        return false
    }
    if(val === "prePath" && this.$store.state.prePath) {
        this.$router.push({path: this.$store.state.prePath});
        return
    }
    if(url && val !== "prePath") {
        this.$router.push({path: url})
    }else {
        this.$router.push({path: "/index"})
    }
}

module.exports = {
    resResult,
    resData,
    resArr,
    delaySubmit,
    navBack
};
