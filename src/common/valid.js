import {Toast} from 'vant';

const valid = {
    phone(data) {
        if(!data) {
            Toast.fail("请输入手机号码");
            return false
        }
        let pattern = /^1\d{10}$/;
        if(pattern.test(data)) {
            return true
        }else {
            Toast.fail("请输入正确的手机号码");
            return false
        }
    },
    password(data) {
        //待优化，6-12位
        if(!data) {
            Toast.fail("请输入密码");
            return false
        }else {
            return true
        }
    },
    smsCode(data) {
        //待优化，4位数字
        if(!data) {
            Toast.fail("请输入验证码");
            return false
        }else {
            return true
        }
    }
};

export default valid
