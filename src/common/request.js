import axios from 'axios';
import router from "../router/index"
import qs from 'qs';
import {Toast} from 'vant';
import util from './util';
import store from '../store/index'


// 设置全局默认的请求头
axios.defaults.headers.Authorization = window.localStorage.getItem('Authorization');
axios.defaults.headers.AppId = window.localStorage.getItem('AppId');
axios.defaults.headers.DeviceType = "WEB";
//
//请求拦截
axios.interceptors.request.use(config=> {
    //设置默认的请求头
    config.headers.Authorization = window.localStorage.getItem('Authorization');
    config.headers.AppId = window.localStorage.getItem('AppId');
    config.headers.DeviceType = "WEB";
    return config;
}, err=> {
    Toast.fail('请求超时！');
    return Promise.resolve(err);
});
//响应拦截
axios.interceptors.response.use(response=> {
    //let curPath = router.app._route.path;
    if (response.status && response.status == 200) {
        //判断jwtToken是否有效
        let loginLose = util.resCodeJudge(response.data.code);
        if(loginLose.permission) {
            let href = window.location.href;
            href = href.split("#")[1];
            store.commit("setPrePath", href);
            router.push("/login");
        }
    }
    return response;
}, err=> {
    let curPath = router.app._route.path;
    if(err && err.response){
        switch (err.response.status) {
            case 400: err.message = '请求错误(400)'; break;
            case 401: err.message = '未授权，请重新登录(401)'; break;
            case 403: err.message = '拒绝访问(403)'; break;
            case 404: err.message = '请求出错(404)'; break;
            case 408: err.message = '请求超时(408)'; break;
            case 500: err.message = '服务器错误(500)'; break;
            case 501: err.message = '服务未实现(501)'; break;
            case 502: err.message = '网络错误(502)'; break;
            case 503: err.message = '服务不可用(503)'; break;
            case 504: err.message = '网络超时(504)'; break;
            case 505: err.message = 'HTTP版本不受支持(505)'; break;
            default: err.message = `连接出错(${err.response.status})！`;
        }
    }else{
        err.message = '连接服务器失败！';
        //退出登录
    }
    Toast.fail({
        message: err.message,
        onClose: () => {
            if(curPath != "/login") {
                router.push("/login");
            }
        }
    });
    return Promise.resolve(err);
});


const request = {
    get(url) {
        return axios({
            method: 'get',
            url: url
        });
    },
    post(url, params) {
        return axios({
            method: 'post',
            url: url,
            data: params
        });
    },
    postForm(url, params) {
        return axios({
            method: 'post',
            url: url,
            data: qs.stringify(params,{arrayFormat: 'repeat'}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    },
    delete(url) {
        return axios({
            method: 'delete',
            url: url,
        });
    },
    upload(url, params) {
        return axios({
            method: 'post',
            url: url,
            data: qs.stringify(params),
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    },
    exportPost: function(url,params) {
        return axios({
            method: 'post',
            url: url,
            data: params,
            responseType: 'blob',
        })
    }
};

export default request
