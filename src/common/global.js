//const path = require("path")
import util from "./util"
const global = {
    size: 20,
    page: 1,
    pages: 1,
    //appId: "wx08daf080df44c615",
    appId: "wx35ee42bc8a4faece",
    isWxBrowser: util.isWxBrowser(),
    swipeAutoplay: 4000,
    swipeDuration: 700
};
export default global
