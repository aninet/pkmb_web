import request from '../common/request';
// let mall = "http://134.205.9.62:8082/mall";
let mall = "http://120.79.211.232:8082/mall";

export default {
    api : {
        //江湖夺宝
        //获取夺宝首页详情
        getTreasureInfo : (p) => request.get(mall+'/treasure/getTreasureInfo',p),
        //夺宝签到
        treasureSign : (p) => request.post(mall+'/treasure/treasureSign/',p),
        //今日夺宝 or 最新开奖
        getTreasureCategory : (p) => request.get(mall + '/treasure/getTreasureCategory/'+p.type),
        //获取夺宝商品列表
        getTreasureGoodsList : (p) => request.post(mall + '/treasure/getTreasureGoodsList/',p),
        //最新开奖商品列表
        getNewTreasureGoodsList : (p) => request.post(mall + '/treasure/getNewTreasureGoodsList/', p),
        //我的夺宝商品列表
        getMyTreasureGoodsList : (p) => request.post(mall + '/treasure/getMyTreasureGoodsList/', p),
        //获取我的福屏卷明细列表
        getFpCouponList : (p) => request.post(mall+'/treasure/getFpCouponList',p),
        //夺宝商品详情
        getTreasureGoodsInfo : p => request.get(mall+'/treasure/getTreasureGoodsInfo/'+p),
        //参与夺宝
        joinTreasure : (p) => request.post(mall+'/treasure/joinTreasure',p),
        //获取我的幸运号
        getMyCode : (p) => request.post(mall+'/treasure/getMyCode/',p),
        //夺宝的用户列表
        getJoinList : (p) => request.post(mall+'/treasure/getJoinList/',p),
        //获取订单夺宝
        receiverInfo : (p) => request.post(mall+'/treasure/receiverInfo/',p),
        //获取夺宝订单详情
        treasureOrderDetails : (p) => request.get(mall+'/treasure/treasureOrderDetails/'+p.id),
        //获取获奖商品弹窗信息
        getOrderNoReceive: (p) => request.post(mall+'/treasure/getOrderNoReceive/',p),
        //获取活动商品的提货地址列表
        getShopAddressList: (p) => request.get(mall+'/treasure/getShopAddressList/'+p.activityGoodsId,{}),
        //查看是否参加过活动
        fpActivity: () => request.get(mall+'/treasure/fpActivity/0'),
        //参与活动888
        joinFpActivity: () => request.get(mall+'/treasure/joinFpActivity/0'),
        //夺宝订单确认收货
        orderTreasureReceiver: p => request.post(mall+'/treasure/orderReceiver/'+p)
    }
};
