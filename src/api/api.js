import request from '../common/request'
import treasure from "./treasure";

let apiType = 4;

let user = "";
let mall = "";
let order = "";

switch (apiType) {
    case 0:
        user = "http://admin.pkmb168.cn/user";
        mall = "http://admin.pkmb168.cn/mall";
        order = "http://test.pkmb168.cn/order";
        break;
    case 1:
        user = "http://134.205.9.62:8080/user";
        order = "http://134.205.9.62:8081/order";
        mall = "http://134.205.9.62:8082/mall";
        break;
    case 2:
        user = "http://test.pkmb168.cn/user";
        order = "http://test.pkmb168.cn/order";
        mall = "http://test.pkmb168.cn/mall";
        break;
    case 3:
        user = "//120.79.211.232:8081/user";
        order = "//120.79.211.232:8083/order";
        mall = "//120.79.211.232:8082/mall";
        break;
    case 4:
        user = "//134.205.9.107:8080/user";
        order = "//134.205.9.107:8081/order";
        mall = "//134.205.9.107:8082/mall";
        break;

    default:
        user = "http://134.205.9.62:8080/user";
        order = "http://134.205.9.62:8081/order";
        mall = "http://134.205.9.62:8082/mall";
}
let api = {
    //登录注册-----------------
    getDynamicSecretKey: () => request.get(user+'/user/getDynamicSecretKey'),//获取动态密钥
    login : p=> request.post(user+'/user/login', p),//手机账号密码登录
    loginWx : p=> request.post(user+'/user/loginWx',p),//微信code登录
    modPassword : p => request.post(user+'/user/modPassword',p),//修改密码
    register : p=> request.post(user+'/user/register', p),//注册
    getUserInfo :  p => request.get(user+'/user/getUserInfo/'+ p),//获取用户信息

    //用户设置
    modHeadPortrait : p => request.post(user+'/user/modHeadPortrait',p), //修改用户头像
    checkLoginStatus : () => request.get(user+'/user/checkLoginStatus'), //校验用户登录状态
    modNickName : p => request.post(user+'/user/modNickName',p), //修改用户昵称
    modSignature : p => request.post(user+'/user/modSignature',p), //修改个性签名
    modSex : p => request.post(user+'/user/modSex',p), //修改性别
    getDistrictList : p=> request.get(user+'/address/getDistrictList/'+p),//获取地址列表
    //saveUserExpress : (p)=> request.post(user+'/address/saveUserExpress/',p), //保存收货地址
    modPayPassword : (p) => request.post(user+'/user/modPayPassword',p),

    //业绩中心
    withdrawApp : p=> request.post(order+'/recharge/withdrawApp',p), //用户提现
    getSales : p => request.post(user+'/userSales/getSales/',p),//获取用户业绩
    getChildren : p => request.post(user+'/userSales/getChildren',p),//获取用户的所有直属下线
    getChildSales : p => request.get(user+'/userSales/getChildSales/'+p),//获取用户下属业绩
    getGradeNum : p => request.get(user+'/userSales/getGradeNum/'+p),//获取用户等级数量

    //发送短信验证码
    resetPasswordGetSmsCode: p=> request.get(user+'/user/getEditPasswordCode/'+ p),// 忘记密码，获取手机验证码
    registerGetSmsCode : p=> request.get(user+'/user/getSmsCode/'+p),//注册，获取手机验证码
    editPayPasswordCode : p=> request.get(user+'/user/getEditPayPasswordCode',p),//获取修改支付密码短信验证码
    modPayPasswordByCode : p=> request.post(user+'/user/modPayPasswordByCode',p),//修改支付密码（短信验证码）

    getEditPayPasswordCode: p=> request.get(user+'/user/getEditPayPasswordCode/', +p),// 不用传手机号码 获取修改支付密码,短信验证码,，登录且绑定手机号才可以修改支付密码

    //首页-----------------------
    getGoodsInfoList : p=> request.post(mall + '/goods/getGoodsInfoList/',p),//首页商品列表
    getBanner:p=>request.get(mall+"/menu/getBannerList/"+p), //获取广告轮播图
    //获取导航按钮
    getNavList:()=>request.get(mall+"/menu/getNavList/"),
    //获取公告列表
    getNoticeList:()=>request.get(mall+"/menu/getNoticeList"),

    //商品
    getGoodsInfo : p=> request.post(mall + '/goods/getGoodsInfo/',p),
    getGoodsStandard : p=> request.get(mall + '/goods/getGoodsStandard/'+p),
    userFavorite : p=> request.post(mall+'/common/userFavorite', p),//收藏商品
    getAllOrderList: p => request.post(order+'/order/getAllOrderList/',p),//获取普通/拼团订单列表
    getOrderDetail : p => request.get(order+'/order/getOrderDetail/'+p),//获取订单详情
    cancelOrder : p => request.get(order+'/order/orderCancel/'+p),//取消订单
    receiveOrder : p => request.get(order+'/order/orderReceiver/'+p),//确认收货
    extendOrder : p => request.get(order+'/order/orderExtend/'+p),//延长收货时间
    getOrderShip : p => request.get(order+'/order/getOrderShippingInfo/'+p),//查看物流
    shippingCost : p => request.post(order+'/shipping/shippingCost',p),//运费

    //商品订单
    goodsOrderCreate : p=> request.post(order+'/order/createOrder/',p),//创建订单
    goodsOrderPay: p => request.post(order+'/order/payOrder/',p),//订单支付

    //礼包订单
    productOrderCreate : p=> request.post(order+'/product/createProductOrder',p),//创建订单
    productPayOrder : p=> request.post(order+'/product/payOrder',p),//创建订单


    //店铺
    getShopInfo : p=> request.get(mall+'/shop/getShopInfo/'+p),//获取商铺详情

    // 获取商品全部分类接口;
    getAllGoodsCategoryList : () => request.get(mall+'/goods/getAllGoodsCategoryList/'),//获取商品所有分类


    //获取余额明细列表
    getBalanceList:p=>request.post(order+'/recharge/moneyList',p),
    //获取提现明细列表
    getWithdrawList:p=>request.post(order+'/recharge/withdrawList',p),
    //获取充值明细列表
    getRechargeList:p=>request.post(order+'/recharge/rechargeList',p),
    //获取账单明细列表
    getBillList:p=>request.post(order+'/recharge/billList',p),

    //礼包
    getProductList:p=>request.post(order+"/product/getProductList",p),
    getProduct:p=>request.get(order+"/product/getProduct/"+p),
    // getProductOrderList:(p)=>request.post(order+"/product/getProductOrderList",p),
    productOrderReceiver:p=>request.post(order+"/product/productOrderReceiver/"+p),
    orderDelete:p=>request.post(order+"/product/orderDelete/"+p),

    //用户---------------------
    //添加收货地址
    //保存收货地址
    saveUserExpress : p=> request.post(user+'/address/saveUserExpress/',p),
    getUserExpress : p=> request.get(user+'/address/getUserExpress',p),//获取用户收货地址
    removeUserExpress : p=> request.post(user+'/address/removeUserExpress/'+p), //删除收货地址
    setDefaultUserExpress : p => request.post(user+'/address/saveDefaultUserExpress/'+p),//设置收货地址为默认地址

    getInviteUserInfo:p=>request.post(user+"/user/getInviteUserInfo/"+p),

    //跑屏相关------------------------------------------------------
    getTaskBannerList : p=> request.get(mall +'/menu/getBannerList/'+p),//跑屏banner
    getTaskSearchTypeList : p=> request.get(mall +'/task/getTaskSearchTypeList/'+p),//任务列表
    getTaskList : p=> request.post(mall +'/task/getTaskList',p),//跑屏列表
    getTaskInfo : p=> request.get(mall +'/task/getTaskInfo/'+p),//跑屏任务详情
    joinTask : p=> request.post(mall +'/task/joinTask', p),//领取任务
    getTaskReceiveList : p=> request.post(mall +'/task/getTaskReceiveList',p),//获取用户跑屏任务列表

    //公共接口
    image : p => request.post(mall+'/common/image',p),
    getJSApiTicket : p => request.post(user+'/common/getJSApiTicket/', p),//微信分享

    getProductOrderList:(p)=>request.post(order+"/product/getProductOrderList",p),
    ...treasure.api,
};

export default api
