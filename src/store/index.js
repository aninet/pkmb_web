import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

/*
*  初始化的几个参数做持久化保存
*  如果以后状态比较复杂，可以引入vuex-persistedstate 对持久化进行管理
* */

/*
* 在使用到 isVip isSetPayPassword myInviteNumber 等数据的时候,
* 请求一次用户数据,确保已登录
*
* */



const store = new Vuex.Store({
    state: {
        keepAliveComponents: [],//状态保持组件
        scrollTop: 0,
        prePath: "/index",//用于指定跳转返回，主要用于登录回调, 特定页面跳转回跳（如选择地址）等
        myInviteNumber: "",//自己的邀请码，在登录的时候获取到
        inviteNum: "", //被邀请，别人的邀请码，点击邀请链接，在url中获取,该命名不可修改，之前的项目用的，商家物料也用的这个
        isVip: false,
        isSetPayPassword: false,
        isBindWx: false,
        phone: "",
        userInfo: {},
        goodsInfo: {},
        shopInfo: {},
        orderInfo: [],
        receiveId: "", // 任务id,在点击分享链接中获得，只维护通过链接进入应用的周期
        tabBar: true,
    },
    mutations: {
        keepAliveSave(state, component) {
            if(state.keepAliveComponents.length >= 3){
                state.keepAliveComponents.shift();
            }
            !state.keepAliveComponents.includes(component) && state.keepAliveComponents.push(component)
        },
        keepAliveDelete(state, component) {
            let index = state.keepAliveComponents.indexOf(component);
            index !== -1 && state.keepAliveComponents.splice(index, 1)
        },
        saveScrollTop(state, height) {
            state.scrollTop = height
        },
        saveUserInfo(state, userInfo) {
            state.userInfo = userInfo;
            state.phone = userInfo.phone;
            state.myInviteNumber = userInfo.inviteNumber;
            state.isVip = (userInfo.isVip == "1");
            state.isSetPayPassword = (userInfo.isSetPayPassword == "1");
            state.isBindWx = (userInfo.isBindWx == "1");
        },
        setPrePath(state, prePath) {
            /*
            * 只因该在登录失效,或者指定跳转的时候调用
            *
            * */
            state.prePath = prePath
        },
        setInviteNum(state, inviteNum) {
            state.inviteNum = inviteNum
        },
        setShopInfo(state, shopInfo) {
            state.shopInfo = shopInfo
        },
        setGoodsInfo(state, goodsInfo) {
            state.goodsInfo = goodsInfo
        },
        setReceiveId(state, receiveId) {
            state.receiveId = receiveId
        },
        setOrderInfo(state, orderInfo) {
            Array.isArray(orderInfo) && (state.orderInfo = orderInfo)
        },
        setTabBar(state, tabBar) {
            state.tabBar = tabBar
        },
        setIsSetPayPassword(state,isSetPayPassword ) {
            state.isSetPayPassword = !!isSetPayPassword
        }
    }

});

export default store
