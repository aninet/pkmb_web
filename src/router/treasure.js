export default [
    {
        path: "/treasure/Index",
        name: "/treasure/index",
        component: () => import(/* webpackChunkName: "treasure" */ '../pages/treasure/Index'),
    },
    //最新开奖
    {
        path: "/treasure/new",
        name: "/treasure/New",
        component: () => import(/* webpackChunkName: "treasure" */ '../pages/treasure/New'),
    },
    //夺宝商品详情
    {
        path: "/treasure/Detail",
        name: "/treasure/Detail",
        component: () => import(/* webpackChunkName: "treasure" */ '../pages/treasure/Detail'),
    },
    //夺宝攻略
    {
        path: "/treasure/help",
        name: "/treasure/help",
        component: () => import(/* webpackChunkName: "treasure" */ '../pages/treasure/Help'),
    },
    // //礼包订单列表
    // {
    //     path: "/treasure/orderList",
    //     name: "/treasure/orderList",
    //     component: () => import(/* webpackChunkName: "/treasure/OrderList" */ '../pages/treasure/OrderList'),
    // },
    // //礼包订单列表
    // {
    //     path: "/treasure/order",
    //     name: "/treasure/order",
    //     component: () => import(/* webpackChunkName: "/treasure/Order" */ '../pages/treasure/Order'),
    // }
]