
import Vue from 'vue'
import Router from 'vue-router'
import Goods from './goods'
import Product from './product'
import Treasure from './treasure'
import Task from './task'
import OfflineShop from './offlineShop'

Vue.use(Router);

const routers =  new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: [
        ...Goods,
        ...Product,
        ...Treasure,
        ...Task,
        ...OfflineShop,
        //登录注册-----------------
        {
            path: "/",
            redirect: "/index"
        },
        {
            path: "/redirectPage",
            name: "redirect",
            component: () => import(/* webpackChunkName: "user" */ '../pages/home/Redirect'),
        },
        {
            path: "/login",
            name: "login",
            component: () => import(/* webpackChunkName: "user" */ '../pages/user/Login'),
        },
        {
            path: "/register",
            name: "register",
            component: () => import(/* webpackChunkName: "user" */ '../pages/user/Register'),
        },
        {
            path: "/wxLogin",
            name: "WxLogin",
            component: () => import(/* webpackChunkName: "user" */ '../pages/user/WxLogin'),
        },
        {
            path: "/passwordReset",
            name: "passwordReset",
            component: () => import(/* webpackChunkName: "user" */ '../pages/user/PasswordReset'),
        },
        //首页-------------------------
        {
            path: "/index",
            name: "index",
            component: () => import(/* webpackChunkName: "user" */ '../pages/home/Index'),
            meta: {
                tabBar : true,
            },
        },
        {
            path: "/category",
            name: "/Category",
            component: () => import(/* webpackChunkName: "category" */ '../pages/home/Category'),
        },
        {
            path: "/categoryChild",
            name: "/CategoryChild",
            component: () => import(/* webpackChunkName: "categoryChild" */ '../pages/home/CategoryChild'),
        },
        {
            path: "/Search",
            name: "/Search",
            component: () => import(/* webpackChunkName: "search" */ '../pages/home/Search'),
        },

        //个人中心------------------
        {
            path: "/personalCenter",
            name: "personalCenter",
            component: () => import(/* webpackChunkName: "user" */ '../pages/userCenter/PersonalCenter'),
            meta: {
                tabBar : true,
            },
        },
        {
            path: "/orderList",
            name: "orderList",
            component: () => import(/* webpackChunkName: "order" */ '../pages/order/OrderList'),
        },
        {
            path: "/orderDetail",
            name: "orderDetail",
            component: () => import(/* webpackChunkName: "order" */ '../pages/order/OrderDetail'),
        },
        {
            path: "/orderShipDetail",
            name: "orderShipDetail",
            component: () => import(/* webpackChunkName: "order" */ '../pages/order/OrderShipDetail'),
        },
        //业绩中心
        {
            path: "/myResult",
            name: "myResult",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/MyResult'),
        },
        //个人中心查看余额
        {
            path: "/remainMoney",
            name: "remainMoney",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/RemainMoney'),
        },
        //我的钱包
        {
            path: "/moneyPackage",
            name: "moneyPackage",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/MoneyPackage'),
        },
        //账单列表
        {
            path: "/billList",
            name: "billList",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/Bill'),
        },
        //账单详情
        {
            path: "/billDetail",
            name: "billDetail",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/BillDetail'),
        },

        //跑屏
        {
            path: "/taskIndex",
            name: "taskIndex",
            component: () => import(/* webpackChunkName: "task" */ '../pages/task/TaskIndex'),
            meta: {
                tabBar : true,
            },
        },
        //余额明细列表
        {
            path: "/remainList",
            name: "remainList",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/RemainList'),
        },
        //余额详情
        {
            path: "/remainDetail",
            name: "remainDetail",
            component: () => import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/RemainDetail'),
        },
        //提现页面
        {
            path:"/withdraw",
            name:"withdraw",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/Withdraw'),
        },
        //提现明细列表
        {
            path:"/withdrawList",
            name:"withdrawList",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/WithdrawList'),
        },
        //提现明细详情
        {
            path:"/withdrawDetail",
            name:"withdrawDetail",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/WithdrawDetail'),
        },
        //充值页面
        {
            path:"/recharge",
            name:"recharge",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/Recharge'),
        },
        //充值明细列表
        {
            path:"/rechargeList",
            name:"rechargeList",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/RechargeList'),
        },
        //充值明细详情
        {
            path:"/rechargeDetail",
            name:"rechargeDetail",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/RechargeDetail'),
        },
        //用户中心设置
        {
            path:"/userSetting",
            name:"userSetting",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/UserSetting'),
        },
        //收货地址
        {
            path:"/addressList",
            name:"addressList",
            component:()=>import(/* webpackChunkName: "address" */ '../pages/address/AddressList'),
        },
        //重置密码
        {
            path:"/resetPassword",
            name:"resetPassword",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/ResetPassword'),
        },
        //业绩中心->我的团队列表
        {
            path:"/groupChildren",
            name:"groupChildren",
            component:()=>import(/* webpackChunkName: "userCenter" */ '../pages/userCenter/GroupChildren'),
        },
        //我的拼团列表
        {
            path:"/group/OrderList",
            name:"/group/OrderList",
            component:()=>import(/* webpackChunkName: "group" */ '../pages/group/OrderList'),
        },
        //拼团订单详情
        {
            path:"/group/OrderDetail",
            name:"/group/OrderDetail",
            component:()=>import(/* webpackChunkName: "group" */ '../pages/group/OrderDetail'),
        },
        {
            path:"/OrderDetailBtn",
            name:"OrderDetailBtn",
            component:()=>import(/* webpackChunkName: "group" */ '../pages/group/OrderDetailBtn'),
        },


    ]
});

export default routers
