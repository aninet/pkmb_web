export default [
    {
        path: "/product/Index",
        name: "/product/index",
        component: () => import(/* webpackChunkName: "product" */ '../pages/product/Index'),
    },
    //礼包详情
    {
        path: "/product/Detail",
        name: "/product/Detail",
        component: () => import(/* webpackChunkName: "product" */ '../pages/product/Detail'),
    },
    //礼包订单列表
    {
        path: "/product/orderList",
        name: "/product/orderList",
        component: () => import(/* webpackChunkName: "product" */ '../pages/product/OrderList'),
    },
    //礼包订单列表
    {
        path: "/product/order",
        name: "/product/order",
        component: () => import(/* webpackChunkName: "product" */ '../pages/product/Order'),
    },
]
