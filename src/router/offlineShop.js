export default [
    {
        path:"/offlineShop/goods/goodsCommon",
        name:"/goodsCommon",
        component: ()=> import(/*webpackChunkName: "offlineShop" */'../pages/offlineShop/goods/GoodsCommon'),
    },
    {
        path:"/offlineShop/goods/testPage",
        name:"/testPage",
        component: ()=> import(/*webpackChunkName: "offlineShop" */'../pages/offlineShop/goods/TestPage'),
    },
]

