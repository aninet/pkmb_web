export default [
    //商品相关路由
    {
        path: "/goods/goodsDetail",
        name: "goodsDetail",
        component: ()=> import(/*webpackChunkName: "goods" */'../pages/goods/GoodsDetail')
    },
    {
        path: "/goodsOrder/orderConfirm",
        name: "orderConfirm",
        component: ()=> import(/*webpackChunkName: "goodsOrder" */'../pages/goods/OrderConfirm'),
        meta: {
            title: '确认订单',
            keepAlive: true,
        },
    },
    //支付成功
    {
        path: "/goodsOrder/paySuccess",
        name: "paySuccess",
        component: ()=> import(/*webpackChunkName: "goodsOrder" */'../pages/goods/PaySuccess'),
    },
]
