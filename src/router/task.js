export default [
    //跑屏详情
    {
        path:"/task/taskDetail",
        name:"/taskDetail",
        component: ()=> import(/*webpackChunkName: "task" */'../pages/task/TaskDetail'),
        meta: {
            tabBar : true,
        },
    },
    //缴纳押金
    {
        path:"/task/taskPayment",
        name:"/taskPayment",
        component: ()=> import(/*webpackChunkName: "task" */'../pages/task/TaskPayment')
    },
    //我的跑屏
    {
        path:"/task/myTaskList",
        name:"/myTaskList",
        component: ()=> import(/*webpackChunkName: "task" */'../pages/task/MyTaskList'),
        meta: {
            tabBar : true,
        },
    },


]
