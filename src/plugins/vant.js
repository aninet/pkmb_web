import Vue from 'vue'
import {
    DatetimePicker,
    Button,
    List,
    Icon,
    Loading,
    NavBar,
    DropdownMenu,
    DropdownItem,
    Toast,
    Image,
    Row,
    Col,
    Divider,
    Card,
    Field,
    PullRefresh,
    Step,
    Steps,
    Tab,
    Tabs,
    Dialog,
    Overlay,
    Picker,
    Popup,
    Radio, Checkbox,RadioGroup,
    Swipe, SwipeItem,
    Cell,
    Search,
    Tabbar, TabbarItem,
    Uploader,
    Sku,
    AddressList,
    Area,
    AddressEdit,
    Stepper ,
    Progress,
} from 'vant';

Vue.use(DatetimePicker)
    .use(Button)
    .use(List)
    .use(Icon)
    .use(NavBar)
    .use(Loading )
    .use(DropdownMenu)
    .use(DropdownItem)
    .use(Toast)
    .use(Image)
    .use(Row)
    .use(Col)
    .use(Divider)
    .use(Card)
    .use(Field)
    .use(PullRefresh )
    .use(Step)
    .use(Steps)
    .use(Tab)
    .use(Tabs)
    .use(Dialog)
    .use(Overlay)
    .use(Picker)
    .use(Popup)
    .use(Radio)
    .use(RadioGroup)
    .use(Checkbox)
    .use(Swipe)
    .use(SwipeItem)
    .use(Cell)
    .use(Search)
    .use(Tabbar)
    .use(TabbarItem)
    .use(Uploader)
    .use(Sku)
    .use(AddressList )
    .use(Area)
    .use(AddressEdit )
    .use(Stepper )
    .use(Progress )

