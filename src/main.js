import Vue from 'vue'
import App from './App.vue'

import lodash from 'lodash'

/*路由中会使用到相关的组件，先引入
* import 'vant/lib/index.css';
* 使用离线图标，默认使用https://img.yzcdn.cn/vant/vant-icon-d3825a.ttf
* import 'vant/lib/icon/local.css';
 */
import './plugins/vant'
//import './assets/icon/iconfont.css' //本地icon, 开发用线上，在index.html 文件 阿里的iconfont
import router from './router/index'
import store from './store/index';
import {
    resResult,
    resData,
    resArr,
    delaySubmit,
    navBack
} from './common/common';
import util from './common/util';
import api from './api/api';
import bus from './common/bus';
import global from './common/global';
import valid from './common/valid';
import './registerComponents/registerComponents';

Vue.prototype.$_ = lodash;
Vue.prototype.$bus = bus;
Vue.prototype.$util = util;
Vue.prototype.$api = api;
Vue.prototype.$resResult = resResult;
Vue.prototype.$resData = resData;
Vue.prototype.$resArr = resArr;
Vue.prototype.$delaySubmit = delaySubmit;
Vue.prototype.$navBack = navBack;
Vue.prototype.$global = global;
Vue.prototype.$valid = valid;
Vue.prototype.$header ={
    "Authorization" : "",
    "AppId" : "",
    "DeviceType" : ""
};

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
    util.routerBeforeFn(to, from, next,store);
    next()
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
