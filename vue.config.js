const path = require("path");
function resolve (dir) {
    return path.join(__dirname, dir)
}
module.exports = {
    devServer: {
        port: 8086, // 端口
    },
    assetsDir: "static",
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.less$/,
                    use: [
                        // ...其他 loader 配置
                        {
                            loader: 'less-loader',
                            options: {
                                modifyVars: {
                                    //通过 less 文件覆盖（文件路径为绝对路径）
                                    'hack': `true; @import "${path.join(
                                        __dirname,
                                        './src/plugins/van-theme.less'
                                    )}";`
                                }
                            }
                        }
                    ]
                }
            ]
        }
    },
    chainWebpack: (config)=>{
        //修改文件引入自定义路径
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@com', resolve('src/common'))
            .set('@components', resolve('src/components'))
            .set('@pages', resolve('src/pages'))
            .set('@style', resolve('src/assets/style'))
            .set('@img', resolve('src/assets/images'))
    }
};
